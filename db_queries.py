import psycopg2

def connection_and_query_execution(dbname, sql_query):
    cursor, connection = establish_connection_with_database(dbname)
    cursor.execute(sql_query)
    result_set = cursor.fetchall()
    print("Data Fetched Successfully")
    return result_set, connection

def establish_connection_with_database(dbname):
    connection = psycopg2.connect(database=dbname, user="postgres", password="test123", host="127.0.0.1", port="5432")
    print("Database opened successfully")
    cursor = connection.cursor()
    return cursor, connection

def find_number_of_matches_played_per_year_using_postgresql(dbname):
    sql_query = "SELECT season, COUNT(season) FROM matches GROUP BY season ORDER BY season"
    result_set, connection = connection_and_query_execution(dbname, sql_query)
    connection.close()
    return dict(result_set)

def calculate_number_of_matches_won_by_each_team_using_postgresql(dbname):
    data = {}
    sql_query = "SELECT season, winner, COUNT(winner) FROM matches GROUP BY season, winner ORDER BY season, winner"
    result_set, connection = connection_and_query_execution(dbname, sql_query)
    connection.close()
    for team in result_set:
        if team[1] not in data and team[1] != None:
            data[team[1]] = [0]*10
            data[team[1]][int(team[0])-2008] += team[2]
        elif team[1] != None:
            data[team[1]][int(team[0])-2008] += team[2]
    return data

def extra_runs_conceded_by_each_team_in_2016_using_postgresql(dbname, year):
    sql_query = f"SELECT deliveries.bowling_team, SUM(deliveries.extra_runs) FROM deliveries INNER JOIN matches ON deliveries.match_id=matches.id and matches.season='{year}' GROUP BY bowling_team"
    result_set, connection = connection_and_query_execution(dbname, sql_query)
    connection.close()
    return dict(result_set)

def extract_top_economical_bowler_in_2015_using_postgresql(dbname, year):
    sql_query = f"SELECT deliveries.bowler, (SUM(deliveries.total_runs)/(COUNT(deliveries.bowler)/6.0)) as run_rate from deliveries INNER JOIN matches ON deliveries.match_id=matches.id and matches.season='{year}' GROUP BY bowler ORDER BY run_rate"
    result_set, connection = connection_and_query_execution(dbname, sql_query)
    connection.close()
    return dict(result_set)

def find_best_batsman_in_2017_using_postgresql(dbname, year):
    sql_query = f"SELECT deliveries.batsman, SUM(deliveries.batsman_runs) from deliveries INNER JOIN matches ON deliveries.match_id=matches.id and matches.season='{year}' GROUP BY batsman"
    result_set, connection = connection_and_query_execution(dbname, sql_query)
    connection.close()
    return dict(result_set)