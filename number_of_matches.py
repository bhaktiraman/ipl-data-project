import csv
from matplotlib import pyplot as plt
from db_queries import find_number_of_matches_played_per_year_using_postgresql


def find_number_of_matches_played_per_year_in_ipl(matches_file_path):
    data = {}
    with open(matches_file_path,'r') as csv_file :
        csv_reader = csv.DictReader(csv_file)
        for match in csv_reader:
            if match['season'] not in data.keys():
                data[match['season']] = 0
            data[match['season']] += 1
    return data 

def plot_number_of_matches_played_per_year_in_ipl(data):
    year = []
    numberOfMatches = []
    for key in data.keys():
        year.append(key)
    for value in data.values():
        numberOfMatches.append(value)

    plt.plot(year,numberOfMatches)
    plt.title('Yearwise Matches')
    plt.xlabel("Year")
    plt.ylabel("Total number of matches")
    plt.show()


def find_and_plot_number_of_matches_played_per_year_in_ipl(matches_file_path):
    data = find_number_of_matches_played_per_year_in_ipl(matches_file_path)
    sql_data = find_number_of_matches_played_per_year_using_postgresql("ipl_db")
    # plot_number_of_matches_played_per_year_in_ipl(data)
    plot_number_of_matches_played_per_year_in_ipl(sql_data)


if __name__ == '__main__':
    find_and_plot_number_of_matches_played_per_year_in_ipl('./matches.csv')