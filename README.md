Data Project - Round 1
=========
Visualization of [IPL Dataset](https://www.kaggle.com/manasgarg/ipl) using [Matplotlib](https://matplotlib.org/) in `Python`

In this data assignment these file's code will transform raw data from IPL into graphs that will convey some meaning / analysis. For each part of this assignment have 2 parts -

Both csv files from [IPL Dataset](https://www.kaggle.com/manasgarg/ipl) is available in this repo:
> matches.csv

> deliveries.csv

also mock .csv files are available in this repo:
> mock_matches.csv 

> mock_deliveries.csv

Codes of python methods that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.

Following plots are in the list:

1. Plot of number of matches played per year of all the years in IPL.
   > number_of_matches.py
2. Plot of matches won of all teams over all the years of IPL in form of _stacked bar chart_
   > stacked_chart_for_matches_won.py
3. Plot of the extra runs conceded per team for the year of 2016.
   > runs_conceded.py
4. Plot of the top economical bowlers for the year of 2015.
   > top_economical_bowlers.py
5. Prepare a data structure of all Best Performing Batsman in the IPL of 2017 and plotted it with matplotlib.
   > best_batsmen.py

And all common methods of all plots are in file `common_methods.py`


> Output (Image) of all above 5 plots are in *.png* file.

> Unittest : all 5 unittest files are in format test_[File_name].py

> SQL code implementation is also done in all files in seperate modules using `Postgresql` (using package psycopg2).
---
