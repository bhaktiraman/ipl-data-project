import csv
from matplotlib import pyplot as plt
from common_methods import retrieve_match_ids_of_season
import numpy as np
from db_queries import extract_top_economical_bowler_in_2015_using_postgresql


def extract_top_economical_bowler_in_2015(matches_file_path, deliveries_file_path, year):
    data = retrieve_match_ids_of_season(matches_file_path, year)
    count_balls = {}
    bowler_data = {}
    with open(deliveries_file_path,'r') as csv_file_deliveries:
        csv_reader = csv.DictReader(csv_file_deliveries)
        for delivery in csv_reader :
            if delivery['match_id'] in data:
                if delivery['bowler'] not in bowler_data:
                    bowler_data[delivery['bowler']] = 0
                    count_balls[delivery['bowler']] = 0
                bowler_data[delivery['bowler']] += int(delivery['total_runs'])
                count_balls[delivery['bowler']] += 1
        csv_file_deliveries.close()
    for key in bowler_data:
        bowler_data[key] = bowler_data[key] / count_balls[key]
    return bowler_data


def plot_top_economical_bowler_in_2015(data):
    bowlerNames = []
    averageRuns = []
    data = sorted(data.items(), key = lambda x : x[1])

    for i in range(len(data)):
        bowlerNames.append(data[i][0])
        averageRuns.append(data[i][1])

    index = np.arange(10)
    plt.bar(index, averageRuns[:10])
    plt.xlabel('Bowler Names', fontsize=10)
    plt.ylabel('Average Runs', fontsize=10)
    plt.xticks(index, bowlerNames[:10], fontsize=6, rotation=60)
    plt.title('Top economical bowlers for the year 2015')
    plt.show()


def extract_and_plot_top_economical_bowler_in_2015(matches_file_path, deliveries_file_path, year):
    data = extract_top_economical_bowler_in_2015(matches_file_path, deliveries_file_path, year)
    sql_data = extract_top_economical_bowler_in_2015_using_postgresql('ipl_db', year)
    # plot_top_economical_bowler_in_2015(data)
    plot_top_economical_bowler_in_2015(sql_data)


if __name__ == '__main__':
    extract_and_plot_top_economical_bowler_in_2015('./matches.csv', './deliveries.csv', 2015)