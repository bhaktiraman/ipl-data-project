import psycopg2

def connect_postgresql_and_create_table():
    connection = psycopg2.connect(database="ipl_db", user="postgres", password="test123", host="127.0.0.1", port="5432")
    print("Database opened successfully")
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE matches(id integer NOT NULL,season varchar,city varchar,date date,team1 varchar,team2 varchar,toss_winner varchar,toss_decision varchar,result varchar,dl_applied integer,winner varchar,win_by_runs integer,win_by_wickets integer,player_of_match varchar,venue varchar,umpire1 varchar,umpire2 varchar,umpire3 varchar)")
    cursor.execute('select * from matches')
    result_set=cursor.fetchall()
    print("Table Created Successfully")
    connection.commit()
    connection.close()
    print(result_set)

if __name__ == '__main__':
    connect_postgresql_and_create_table()