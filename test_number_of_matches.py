import unittest
from number_of_matches import find_number_of_matches_played_per_year_in_ipl

class TestNumberOfMatches(unittest.TestCase):
    def test_find_number_of_matches_played_per_year_in_ipl(self):
        expected_output = {'2017':1, '2016':2, '2015': 2}
        output = find_number_of_matches_played_per_year_in_ipl('./mock_matches.csv')
        self.assertEqual(output, expected_output)

if __name__ == '__main__':
    unittest.main()