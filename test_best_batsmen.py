import unittest
from best_batsmen import find_and_plot_best_batsman_in_2017

class TestBestBatsmen(unittest.TestCase):
    def test_find_best_batsman_in_2017(self):
        expected_output = {'DA Warner': 6, 'CH Gayle': 4, 'Mandeep Singh': 2}
        output = find_best_batsman_in_2017('mock_matches.csv', 'mock_deliveries.csv', 2017)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()