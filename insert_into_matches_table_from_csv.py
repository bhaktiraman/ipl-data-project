import psycopg2

def connect_postgresql_and_insert_into_table():
    connection = psycopg2.connect(database="ipl_db", user="postgres", password="test123", host="127.0.0.1", port="5432")
    print("Database opened successfully")
    cursor = connection.cursor()
    cursor.execute("COPY matches FROM '/home/vicky/Desktop/Projects/ipl-data-project/matches.csv' DELIMITER ',' CSV HEADER;")
    cursor.execute('select * from matches')
    result_set=cursor.fetchall()
    print("Data Inserted Successfully")
    connection.commit()
    connection.close()
    print(result_set)

if __name__ == '__main__':
    connect_postgresql_and_insert_into_table()