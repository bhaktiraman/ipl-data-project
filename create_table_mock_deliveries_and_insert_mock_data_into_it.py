import psycopg2

def connect_postgresql_to_create_table_and_insert_data_into_it():
    connection = psycopg2.connect(database="ipl_test_db", user="postgres", password="test123", host="127.0.0.1", port="5432")
    print("Database opened successfully")
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE mock_deliveries(match_id integer NOT NULL, inning integer, batting_team varchar, bowling_team	varchar, over integer, ball	integer, batsman	varchar, non_striker varchar, bowler varchar, is_super_over	integer, wide_runs integer, bye_runs integer, legbye_runs integer,	noball_runs integer, penalty_runs integer, batsman_runs	integer, extra_runs	integer, total_runs	integer, player_dismissed varchar,	dismissal_kind varchar, fielder varchar)")
    cursor.execute("COPY mock_deliveries FROM '/home/vicky/Desktop/Projects/ipl-data-project/mock_deliveries.csv' DELIMITER ',' CSV HEADER;")
    cursor.execute('select * from mock_deliveries')
    result_set=cursor.fetchall()
    print("Table Created Successfully")
    connection.commit()
    connection.close()
    print(result_set)

if __name__ == '__main__':
    connect_postgresql_to_create_table_and_insert_data_into_it()