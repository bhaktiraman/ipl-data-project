import unittest
from stacked_chart_for_matches_won import calculate_number_of_matches_won_by_each_team

class TestBarChartOfMatchesWon(unittest.TestCase):
    def test_calculate_number_of_matches_won_by_each_team(self):
        expected_output = {'Sunrisers Hyderabad': [0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 'Rising Pune Supergiant': [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 'Kolkata Knight Riders': [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 'Kings XI Punjab': [0, 0, 0, 0, 0, 0, 0, 1, 0, 0], 'Royal Challengers Bangalore': [0, 0, 0, 0, 0, 0, 0, 1, 0, 0]}
        output = calculate_number_of_matches_won_by_each_team('mock_matches.csv')
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()