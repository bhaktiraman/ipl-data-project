import psycopg2

def connect_sql_and_fetch_data_from_table():
    connection = psycopg2.connect(database="ipl_test_db", user="postgres", password="test123", host="127.0.0.1", port="5432")
    print("Database opened successfully")
    cursor = connection.cursor()
    cursor.execute('select * from matches')
    result_set=cursor.fetchall()
    print("Data Fetched Successfully")
    connection.close()
    print(result_set)

if __name__ == '__main__':
    connect_sql_and_fetch_data_from_table()
