import unittest
from runs_conceded import extra_runs_conceded_by_each_team_in_2016

class TestRunsConceded2016(unittest.TestCase):
    def test_find_number_of_matches_played_per_year_in_ipl(self):
        expected_output = {'Rising Pune Supergiant': 1, 'Mumbai Indians': 2, 'Kolkata Knight Riders': 1, 'Gujarat Lions': 0}
        output = extra_runs_conceded_by_each_team_in_2016('mock_matches.csv', 'mock_deliveries.csv')
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()