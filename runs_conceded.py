import csv
from matplotlib import pyplot as plt
from common_methods import retrieve_match_ids_of_season
from db_queries import extra_runs_conceded_by_each_team_in_2016_using_postgresql
import numpy as np


def extra_runs_conceded_by_each_team_in_2016(matches_file_path, deliveries_file_path, year):
    data = retrieve_match_ids_of_season(matches_file_path, year)
    bowling_team_data = {}
    with open(deliveries_file_path,'r') as csv_file_deliveries:
        csv_reader = csv.DictReader(csv_file_deliveries)
        for delivery in csv_reader :
            if delivery['match_id'] in data:
                if delivery['bowling_team'] not in bowling_team_data:
                    bowling_team_data[delivery['bowling_team']] = 0
                bowling_team_data[delivery['bowling_team']] += int(delivery['extra_runs'])
    return bowling_team_data

def plot_extra_runs_conceded_by_each_team_in_2016(data):
    teamNames = list(data.keys())
    runs = list(data.values())
    index = np.arange(len(teamNames))
    plt.bar(index, runs)
    plt.xlabel('Bowling Team Names', fontsize=5)
    plt.ylabel('Runs conceded', fontsize=5)
    plt.xticks(index, teamNames, fontsize=5, rotation=30)
    plt.title('Extra runs conceded per team for the year 2016')
    plt.show()


def calculate_and_plot_extra_runs_conceded_by_each_team_in_2016(matches_file_path, deliveries_file_path, year):
    result = extra_runs_conceded_by_each_team_in_2016(matches_file_path, deliveries_file_path, year)
    sql_result = extra_runs_conceded_by_each_team_in_2016_using_postgresql('ipl_db', year)
    # plot_extra_runs_conceded_by_each_team_in_2016(result)
    plot_extra_runs_conceded_by_each_team_in_2016(sql_result)


if __name__ == '__main__':
    calculate_and_plot_extra_runs_conceded_by_each_team_in_2016('./matches.csv', './deliveries.csv', 2016)