import unittest
from top_economical_bowlers import extract_top_economical_bowler_in_2015

class TestTopEconomicalBowlers(unittest.TestCase):
    def test_extract_top_economical_bowler_in_2015(self):
        expected_output = {'Sandeep Sharma': 0.16666666666666666, 'AB Dinda': 0.0, 'Z Khan': 0.8333333333333333, 'B Stanlake': 0.16666666666666666}
        output = extract_top_economical_bowler_in_2015('mock_matches.csv', 'mock_deliveries.csv', 2015)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()