from matplotlib import pyplot as plt
import csv
from db_queries import calculate_number_of_matches_won_by_each_team_using_postgresql


def calculate_number_of_matches_won_by_each_team(matches_file_path):
    data = {}
    with open(matches_file_path,'r') as csv_file :
        csv_reader = csv.DictReader(csv_file)
        for line in csv_reader:
            if line['winner'] != '':
                if line['winner'] not in data:
                    data[line['winner']] = [0]*10
                    data[line['winner']][int(line['season'])-2008] += 1
                else:
                    data[line['winner']][int(line['season'])-2008] += 1
    return data

def plot_number_of_matches_won_by_each_team(data):
    print(data)
    space_in_bottom = [0] *10
    team_names = [ key for key in data.keys() ]
    list_of_data = [ val for val in data.values() ]
    year_list = []
    x_axis_positions = []
    p = []
    for i in range(0,10):
        x_axis_positions.append(i)
        year_list.append(2008+i)

    for i in range(0,14):
        plt.bar(x_axis_positions, list_of_data[i], bottom=space_in_bottom , width = 0.45)
        space_in_bottom = [space_in_bottom[j] + list_of_data[i][j] for j in range(0,10)]
        

    plt.ylabel('Number of matches')
    plt.xlabel('Years')
    plt.title('Yearwise Matches Won')
    plt.xticks(x_axis_positions, year_list)
    plt.legend(team_names, loc = "upper right", fontsize = 8, bbox_to_anchor = (1.15, 1))
    plt.show()


def calculate_and_plot_number_of_matches_won_by_each_team(matches_file_path):
    data = calculate_number_of_matches_won_by_each_team(matches_file_path)
    sql_data = calculate_number_of_matches_won_by_each_team_using_postgresql('ipl_db')
    # plot_number_of_matches_won_by_each_team(data)
    plot_number_of_matches_won_by_each_team(sql_data)


if __name__ == '__main__':
    calculate_and_plot_number_of_matches_won_by_each_team('./matches.csv')