import csv

def retrieve_match_ids_of_season(matches_file_path, year):
    match_ids = {}
    with open(matches_file_path,'r') as csv_file_matches:
        csv_reader = csv.DictReader(csv_file_matches)
        for match in csv_reader:
            if match['season'] == str(year):
                if match['id'] not in match_ids :
                    match_ids[match['id']] = 0
    return match_ids
