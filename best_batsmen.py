import csv
from matplotlib import pyplot as plt
from common_methods import retrieve_match_ids_of_season
import numpy as np
from db_queries import find_best_batsman_in_2017_using_postgresql


def find_best_batsman_in_2017(matches_file_path, deliveries_file_path, year):
    data = retrieve_match_ids_of_season(matches_file_path, year)
    batsman_data = {}
    with open(deliveries_file_path,'r') as csv_file_deliveries:
        csv_reader = csv.DictReader(csv_file_deliveries)
        for delivery in csv_reader :
            if delivery['match_id'] in data:
                if delivery['batsman'] not in batsman_data:
                    batsman_data[delivery['batsman']] = 0
                batsman_data[delivery['batsman']] += int(delivery['batsman_runs'])
    return batsman_data


def plot_best_batsman_in_2017(data):
    batsmanNames = []
    runs = []
    data = sorted(data.items(), key = lambda x : x[1], reverse=True)

    for i in range(len(data)):
        batsmanNames.append(data[i][0])
        runs.append(data[i][1])

    index = np.arange(10)
    plt.bar(index, runs[:10])
    plt.xlabel('Batsman Names', fontsize=10)
    plt.ylabel('Runs', fontsize=10)
    plt.xticks(index, batsmanNames[:10], fontsize=6, rotation=60)
    plt.title('Best batsman for the year 2017')
    plt.show()


def find_and_plot_best_batsman_in_2017(matches_file_path, deliveries_file_path, year):
    data = find_best_batsman_in_2017(matches_file_path, deliveries_file_path, year)
    sql_data = find_best_batsman_in_2017_using_postgresql('ipl_db', year)
    # plot_best_batsman_in_2017(data)
    plot_best_batsman_in_2017(sql_data)
    

if __name__ == '__main__':
    find_and_plot_best_batsman_in_2017('./matches.csv', './deliveries.csv',2017)